import UIKit
import QuartzCore

class CreateContactViewController: BaseViewController {

    @IBOutlet weak var fullnameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var viewModel: CreateContactViewModel?
    
    override func configureUI() {
        
        navigationItem.title = navigationBarTitle
        saveButton.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        
        phoneTextField.keyboardType = .phonePad
        
        fullnameTextField.delegate = self
        phoneTextField.delegate = self
        
        fullnameTextField.layer.borderWidth = 1
        phoneTextField.layer.borderWidth = 1
        
    }
    
    override func configureTemplate() {
        
        view.backgroundColor = UIColor.orange
    }
    
    func saveAction() {
        
        guard let viewModel = viewModel else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        
        fullnameTextField.resignFirstResponder()
        phoneTextField.resignFirstResponder()
        
        guard let name = fullnameTextField.text, viewModel.checkInput(name) else {
            
            UIAlertView(title: Constant.Alert.Warning, message: Constant.Alert.NameMessage, delegate: nil, cancelButtonTitle: Constant.Alert.Ok).show()
            return
        }
        
        guard let phone = phoneTextField.text, viewModel.checkInput(name) else {
            
            UIAlertView(title: Constant.Alert.Warning, message: Constant.Alert.PhoneMessage, delegate: nil, cancelButtonTitle: Constant.Alert.Ok).show()
            return
        }
        
        viewModel.addContact(withName: name, phone: phone) { resultTuple in
            
            if let title = resultTuple.0,
               let message = resultTuple.1 {
                
                UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: Constant.Alert.Ok).show()
            }
        }
    }
    
}
    
extension CreateContactViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let viewModel = viewModel else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        
        if let text = textField.text {
            
            let newString =  NSString(string: text).replacingCharacters(in: range, with: string)
            if viewModel.checkInput(newString) {
                
                textField.layer.borderColor = UIColor.green.cgColor
            } else {
                
                textField.layer.borderColor = UIColor.red.cgColor
            }
        }
        
        return true
    }
}
