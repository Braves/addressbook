import UIKit

class ContactDetailViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: ContactDetailViewModel? {
        
        willSet {
            
            viewModel?.viewDelegate = nil
        }
        didSet {
            
            viewModel?.viewDelegate = self
        }
    }
 
    override func configureTemplate() {
        
        view.backgroundColor = UIColor.red
        tableView.backgroundColor = UIColor.clear
    }
    
    override func configureUI() {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        viewModel.setup()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: Constant.Cell.ContactDetailHeaderView, bundle: nil), forHeaderFooterViewReuseIdentifier: Constant.Cell.ContactDetailHeaderView)
        tableView.register(UINib(nibName: Constant.Cell.ContactDetailCell, bundle: nil), forCellReuseIdentifier: Constant.Cell.ContactDetailCell)
       
        if #available(iOS 10.0, *) {
            tableView.refreshControl = UIRefreshControl()
            tableView.refreshControl?.backgroundColor = UIColor.white
            tableView.refreshControl?.tintColor = UIColor.black
            tableView.refreshControl?.addTarget(self, action: #selector(reload), for: .valueChanged)
        }
        
        navigationItem.title = navigationBarTitle
    }
    
    override func updateView() {
        
        tableView.reloadData()
    }
   
    func reload() {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        viewModel.reload()
    }
    
    func call() {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        
        if let number = viewModel.phonNumber() {
        
            let url = URL(fileURLWithPath: "tel://\(number)")
            
            if UIApplication.shared.canOpenURL(url) { // simulator can't make calls 😸 - use phone
                
                UIApplication.shared.openURL(url) 
            } else {
                
                UIAlertView(title: Constant.Alert.Warning, message: Constant.Alert.NotCallMessage, delegate: nil, cancelButtonTitle: Constant.Alert.Ok).show()
            }
            
        } else {
            
            UIAlertView(title: Constant.Alert.Warning, message: Constant.Alert.NotPhoneMessage, delegate: nil, cancelButtonTitle: Constant.Alert.Ok).show()
        }
    }
}

extension ContactDetailViewController: ContactDetailViewModelViewDelegate {
    
    func updateContent(viewModel: ViewModelProtocol) {
       
        if #available(iOS 10.0, *) {
            tableView.refreshControl?.endRefreshing()
        }
        
        updateView()
    }
}

extension ContactDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        
        if viewModel.count() > 0 {
            
            return 63.0
        }
        
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        viewModel.selectEntry(at: indexPath.row)
    }
}

extension ContactDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.Cell.ContactDetailCell, for: indexPath) as? ContactDetailTableViewCell else { fatalError(Constant.ErrorMessage.CellRegistration.rawValue)
        }
        
        cell.detailNameLabel.text = viewModel.name(at: indexPath.row) ?? ""
        cell.detailQuantityLabel.text = viewModel.count(at: indexPath.row) ?? ""
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }        
        return viewModel.count()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section != 0 { return nil }
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constant.Cell.ContactDetailHeaderView) as? ContactDetailHeaderView else { fatalError(Constant.ErrorMessage.CellRegistration.rawValue)
        }
        
        view.phoneLabel.text = "Phone"
        view.phoneNumberLabel.text = viewModel.phonNumber() ?? "-"

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(call))
        view.addGestureRecognizer(recognizer)
        
        return view
    }
}
