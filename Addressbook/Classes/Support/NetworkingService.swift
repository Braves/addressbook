import Foundation
import Alamofire
import AlamofireImage

protocol NetworkingProtocol: class {
    
    func getList(withBlock: @escaping (Data?)->())
    func getImages(for url: String?, withBlock block: @escaping (Data?)->())
    func getDetail(by userId: String?, withBlock block: @escaping (Data?)->())
    func post(_ parameters:[String: String]?, withBlock block: @escaping (Data?)->())
}

class NetworkingService: NetworkingProtocol {
    
    func getList(withBlock block: @escaping (Data?)->()) {
        
        let urlString = Constant.API.GET.ContactList
        
        if let url = URL(string: urlString) {
        
            makeRequest(URLRequest(url: url)) { data in
                
                if let data = data {
                    block(data)
                } else {
                    print("nil in response")
                }
            }
        }
    }
    
    func getImages(for url: String?, withBlock block: @escaping (Data?)->()) {
        
        if let url = url {
            // Oh no! No https -> no ATS  🙀🙀🙀
            Alamofire.request(url).responseImage { response in
                debugPrint(response)
                
                if let image = response.result.value {
                    print("image downloaded: \(image)")
                    block(response.data)
                }
            }        
        }
    }
    
    func getDetail(by userId: String?, withBlock block: @escaping (Data?)->()) {
        
        if let id = userId {
            
            let urlString = [Constant.API.GET.ContactDetail, id].joined(separator: "/")
            
            if let url = URL(string: urlString) {
            
                makeRequest(URLRequest(url: url)) { data in
                
                    if let data = data {
                        
                        block(data)
                    } else {
                        fatalError("Smth went wrong!")
                    }
                }
            }
        }
    }
    
    func post(_ parameters:[String: String]?, withBlock block: @escaping (Data?)->()) {
    
        if let params = parameters {
            
            if let urlRequest = buildPostRequest(params) {
                
                makeRequest(urlRequest) { data in
                
                    if let data = data {
                        
                        block(data)
                    } else {
                        fatalError("Smth went wrong!")
                    }
                }
            }
        }
    }
    
    private func buildPostRequest(_ params:[String: String]?) -> URLRequest? {
        
        if let params = params {
            
            let url = URL(string: Constant.API.POST.CreateContact)!
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "POST"
            
            do {
                
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
            } catch {
                
                fatalError("Smth went wrong!")
            }
            
            urlRequest.setValue(Constant.API.ContentType, forHTTPHeaderField: "Content-Type")
            
            return urlRequest
        }
        
        return .none
    }
    
    private func makeRequest(_ request: URLRequest?, withBlock block: @escaping (Data?)->()) {
        
        if var request = request {
            
            request.cachePolicy = .useProtocolCachePolicy
            
            Alamofire.request(request).responseJSON { response in
                
                if let error = response.result.error {
                    
                    print(error.localizedDescription)
                    return
                }
                
                block(response.data)
            }
        }
    }
}
