import Foundation
import SwiftyJSON

protocol MappingServiceProtocol: class {    
    
    func deserializeListOfContacts(_ data: Data) -> [ModelProtocol]?
    func deserializeContactImage(_ data: Data) -> Data?
    func deserializeContactDetail(_ data: Data) -> [ModelProtocol]?
    func deserializeNewContact(_ data: Data) -> (Constant.ServerResponse, String?)
}

class MappingService: MappingServiceProtocol {
  
    func deserializeListOfContacts(_ data: Data) -> [ModelProtocol]? {
        
        let json = JSON(data: data)
        let items = json["items"]
        guard let contactArray = items.array else { return .none }
        var models = [ModelProtocol]()
        for item in contactArray {
            
            let model = Contact(item["id"].string,
                                          name: item["name"].string,
                                          phone: item["phone"].string,
                                          kind: item["kind"].string,
                                          pictureUrl: item["pictureUrl"].string)
            models.append(model)
        }
        
        return models
    }
    
    func deserializeContactImage(_ data: Data) -> Data? {
        
        return data
    }
   
    func deserializeContactDetail(_ data: Data) -> [ModelProtocol]? {
        
        let json = JSON(data: data)
        let items = json["items"]
        guard let contactArray = items.array else { return .none }
        var models = [ModelProtocol]()
        
        for item in contactArray {
            let model = OrderItem(item["name"].string,
                                count: item["count"].int,
                                kind: item["kind"].string)
            models.append(model)
        }
        
        return models
    }
    
    func deserializeNewContact(_ data: Data) -> (Constant.ServerResponse, String?) {
        
        let json = JSON(data: data)
        
        if let _ = json["id"].string {
            
            return (Constant.ServerResponse.Success, Constant.ServerMessage.SuccessMessage.rawValue)
        }
        
        return (Constant.ServerResponse.Error, "null")
    }
}
