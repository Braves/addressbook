/**
 
 Usually I do not prefer Global Constants
 I prefer Config.plist or put constant strings to related files.
 But for test task The Constant struct was created for code review
 
 **/

struct Constant {
    
    private static let description = "Constant"
    
    enum ErrorMessage: String {
        case MethodOverride = "Should be overriden in child!"
        case ObjectNotFound = "Object was not found!"
        case CellRegistration = "Cell was not registered!"
        case Unknown = "Something went wrong!"
    }
    
    enum ServerResponse: String {
        case Success
        case Error
    }
    
    enum ServerMessage: String {
        case SuccessMessage = "New Contact has been added!"
        case ErrorMessage = "Oops! Some troubles: "
    }

    struct Screen {
        private static let description = "Screen"
        
        static let ContactListTitle = "Contacts"
        static let CreateContactTitle = "Add New Contact"
    }
    
    struct Cell {
        private static let description = "Cell"
        
        static let ContactListCell = "ContactListTableViewCell"
        static let ContactDetailHeaderView = "ContactDetailHeaderView"
        static let ContactDetailCell = "ContactDetailTableViewCell"
    }
    
    struct Config {
        private static let description = "Config"
        
        static let StoryboardName = "Addressbook"
        static let ContactListViewControllerIdentifier = "ContactListViewControllerIdentifier"
        static let ContactDetailViewControllerIdentifier = "ContactDetailViewControllerIdentifier"
        static let CreateContactViewControllerIdentifier = "CreateContactViewControllerIdentifier"
    }
    
    struct Alert {
        private static let description = "Alert"
        
        static let Warning = "Warning!"
        static let Ok = "Ok"
        static let NameMessage = "\"Name\" field should contain at least 5 characters!"
        static let PhoneMessage = "\"Phone\" field should contain at least 5 characters!"
        static let NotCallMessage = "Couldn't call!"
        static let NotPhoneMessage = "No phone number!"
    }
    
    struct API {
        private static let description = "API"
        
        static let Base = "https://inloop-contacts.appspot.com/_ah/api"
        static let ContentType = "application/json"
        
        struct GET {
            private static let description = "GET"
            
            static let ContactList = [Constant.API.Base,"contactendpoint/v1/contact"].joined(separator: "/")
            static let ContactDetail = [Constant.API.Base, "orderendpoint/v1/order"].joined(separator: "/")
        }

        struct POST {
            private static let description = "POST"
            
            static let CreateContact = [Constant.API.Base, "contactendpoint/v1/contact"].joined(separator: "/")
        }
    }
}
