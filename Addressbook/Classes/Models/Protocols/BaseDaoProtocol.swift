import Foundation

protocol BaseDaoProtocol: class {
    
    var model: ModelProtocol { get set }
}
