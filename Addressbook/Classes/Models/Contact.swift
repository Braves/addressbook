import Foundation

class Contact: ModelProtocol {
    
    fileprivate var identifier: String?
    fileprivate var name: String?
    fileprivate var phone: String?
    fileprivate var kind: String?
    fileprivate var pictureUrl: String?
    fileprivate var cart: [ModelProtocol]?
    fileprivate var picture: Data?
    
    init(_ identifier: String?, name: String?, phone:String?, kind: String?, pictureUrl: String?) {
        
        self.identifier = identifier
        self.name = name
        self.phone = phone
        self.kind = kind
        self.pictureUrl = pictureUrl
    }
    
    init(_ name: String?, phone:String?) {

        self.name = name
        self.phone = phone
    }
    
    lazy var dao: ContactDao = {
        
        return ContactDao(self)
    }()
}

class ContactDao: BaseDaoProtocol {

    var model: ModelProtocol
    
    init(_ model: Contact) {
        
        self.model = model
    }
    
    func identifier() -> String? {
        
        if let model = model as? Contact {
            
            return model.identifier
        }
        
        return .none
    }
    
    func name() -> String? {
        
        if let model = model as? Contact {
            
            return model.name
        }
        
        return .none
    }
  
    func phone() -> String? {
        
        if let model = model as? Contact {
            
            return model.phone
        }
        
        return .none
    }
    
    func kind() -> String? {
        
        if let model = model as? Contact {
            
            return model.kind
        }
        
        return .none
    }
    
    func pictureUrl() -> String? {
        
        if let model = model as? Contact {
            
            return model.pictureUrl
        }
        
        return .none
    }
    
    func cart(with items: [ModelProtocol]?) {
        
        if let model = model as? Contact, let items = items {
            
            model.cart = items
        }
    }
    
    func cart() -> [OrderItem]? {
        
        if let model = model as? Contact {
            
            if let cart = model.cart as? [OrderItem] {
                
                return cart
            }
        }
        
        return .none
    }
    
    func picture(with data: Data?) {
        
        if let model = model as? Contact {
            
            model.picture = data
        }
    }
   
    func picture() -> Data? {
        
        if let model = model as? Contact {
            
            return model.picture
        }
        
        return .none
    }
    
}
