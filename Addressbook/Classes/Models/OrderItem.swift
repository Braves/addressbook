import Foundation

class OrderItem: ModelProtocol {
    
    fileprivate var name: String?
    fileprivate var count: Int?
    fileprivate var kind: String?
    
    init(_ name: String?, count: Int?, kind: String?) {
        
        self.name = name
        self.count = count
    }
    
    lazy var dao: OrderItemDao = {
        
        return OrderItemDao(self)
    }()
}

class OrderItemDao: BaseDaoProtocol {
    
    var model: ModelProtocol
    
    init(_ model:OrderItem) {
        
        self.model = model
    }
    
    func name() -> String? {
        
        if let model = model as? OrderItem {
            
           return model.name
        }
        
        return .none
    }

    func count() -> String? {
        
        if let model = model as? OrderItem {
            
            if let count = model.count {
                
                return String(count)
            }
        }
        
        return .none
    }

    func kind() -> String? {
        
        if let model = model as? OrderItem {
            
            return model.kind
        }
        
        return .none
    }

}
