import UIKit

class ContactDetailHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
}
