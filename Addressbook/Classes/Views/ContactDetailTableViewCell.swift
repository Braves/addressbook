import UIKit

class ContactDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var detailNameLabel: UILabel!
    @IBOutlet weak var detailQuantityLabel: UILabel!
    
}
