import Foundation

class BaseViewModel: ViewModelProtocol {

    lazy var networkingService: NetworkingProtocol? = { return NetworkingService() }()
    lazy var mappingService: MappingServiceProtocol? = { return MappingService() }()
    var parentViewModel: ViewModelProtocol?
    
    required init(withParentViewModel viewModel: ViewModelProtocol?) {
        
        self.parentViewModel = viewModel
    }
    
    func setup() {
        
        fatalError( Constant.ErrorMessage.MethodOverride.rawValue )
    }
}
