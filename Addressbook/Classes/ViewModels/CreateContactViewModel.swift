import Foundation

protocol CreateContactViewModelViewDelegate: class {}

protocol CreateContactViewModelDelegate: class {
    
    func saveWithViewModel(_ :ViewModelProtocol)
}

class CreateContactViewModel: BaseViewModel {
    
    weak var viewDelegate: CreateContactViewModelViewDelegate?
    weak var coordinatorDelegate: CreateContactViewModelDelegate?
    
    override func setup() {
        
    }
    
    private func correctNumberOfCharacters() -> Int {
        return 5
    }
    
    func checkInput(_ value: String?) -> Bool {
        
        if let value = value, value.characters.count >= correctNumberOfCharacters() {
            
            return true
        }
        
        return false
    }
    
    func addContact(withName name: String?, phone: String?, result: @escaping ((String?, String?))->()) {
        
        if let networkingService = networkingService,
            let mappingService = mappingService,
            let name = name,
            let phone = phone {
            
            let parameters = [ "name" : name, "phone" : phone]
            
            networkingService.post(parameters,
                                               withBlock: { [unowned self, unowned mappingService] (data) in
                
                if let data = data {
                    
                    let response = mappingService.deserializeNewContact(data)
                    
                    switch response.0 {
                    case .Error:
                        result((Constant.ServerResponse.Error.rawValue, "\(Constant.ServerMessage.ErrorMessage.rawValue) \(response.1) ☹️"))
                        return
                    default:
                        break
                    }
                    
                    result((Constant.ServerResponse.Success.rawValue, Constant.ServerMessage.SuccessMessage.rawValue))
                    
                    if let coordinatorDelegate = self.coordinatorDelegate {
                        
                        coordinatorDelegate.saveWithViewModel(self)
                    }
                    return
                    
                } else {
                    
                    result((Constant.ServerResponse.Error.rawValue, "\(Constant.ServerMessage.ErrorMessage.rawValue) no data"))
                    return
                }
            })
        }
    }
    
}
