import Foundation

protocol ContactDetailViewModelViewDelegate: class {
    
    func updateContent(viewModel: ViewModelProtocol)
}

protocol ContactDetailViewModelDelegate: class {}

class ContactDetailViewModel: BaseViewModel {
    
    weak var viewDelegate: ContactDetailViewModelViewDelegate?
    weak var coordinatorDelegate: ContactDetailViewModelDelegate?
        
    var model: ModelProtocol? {
        
        didSet {
            
            if let viewDelegate = self.viewDelegate {
                
                viewDelegate.updateContent(viewModel: self)
            }
        }
    }
    
    override func setup() {
        
        if let parentViewModel = parentViewModel as? ContactListViewModel,
            let networkingService = networkingService,
            let mappingService = mappingService,
            let selected = parentViewModel.selectedModel,
            let model = selected as? Contact {
            
            if let cart = model.dao.cart(),
                cart.count > 0 {
                
                self.model = model
            } else {
                
                networkingService.getDetail(by: model.dao.identifier(),
                                            withBlock: { [unowned self, unowned mappingService] response in
                                                
                    if let response = response {
                        
                        model.dao.cart(with: mappingService.deserializeContactDetail(response))
                        self.model = model
                    }
                })
            }
        }
    }
    
    func reload() {
        
        if let model = model as? Contact {
            
            model.dao.cart(with: [])
            setup()
        }
    }
    
    func selectEntry(at index: Int) {        
        //handle selection
    }
    
    func name(at index: Int) -> String? {
        
        if let model = model as? Contact,
            let items = model.dao.cart(),
            items.count > 0 {
            
            return items[index].dao.name()
        }
        
        return .none
    }
    
    func count(at index: Int) -> String? {
        
        if let model = model as? Contact,
            let items = model.dao.cart(),
            items.count > 0 {
            
            return items[index].dao.count()
        }
        return .none
    }
    
    func count() -> Int {
        
        if let model = model as? Contact,
            let items = model.dao.cart(),
            items.count > 0 {
            
            return items.count
        }
        
        return 0
    }
    
    func phonNumber() -> String? {
        
        if let model = model as? Contact {
            
            return model.dao.phone()
        }
        
        return .none
    }
}
