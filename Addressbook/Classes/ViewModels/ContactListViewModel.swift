import Foundation

protocol ContactListViewModelViewDelegate: class {
    
    func updateContent(viewModel: ViewModelProtocol)
}

protocol ContactListViewModelDelegate: class {
    
    func addNewContactAction(_ viewModel: ViewModelProtocol)
    func selectContactAction(_ viewModel: ViewModelProtocol)
}

class ContactListViewModel: BaseViewModel {
    
    weak var viewDelegate: ContactListViewModelViewDelegate?
    weak var coordinatorDelegate: ContactListViewModelDelegate?
    var selectedModel: ModelProtocol?
    
    var models: [ModelProtocol]? {
        
        didSet {
            
            if let viewDelegate = self.viewDelegate {
                
                viewDelegate.updateContent(viewModel: self)
                self.performAdditionalUpdates()
            }
        }
    }
    
    override func setup() {
        
        if let networkingService = networkingService,
            let mappingService = mappingService {
            
            networkingService.getList{ [unowned self, unowned mappingService] response in
                
                if let response = response {
                    
                    self.models = mappingService.deserializeListOfContacts(response)
                }
            }
        }
    }
    
    func performAdditionalUpdates() {
        
        if let models = models,
            let networkingService = networkingService,
            let mappingService = mappingService,
            let viewDelegate = viewDelegate {
            
            models.forEach{ [unowned self, unowned networkingService, unowned viewDelegate] model in
                
                if let contact = model as? Contact {
                
                    networkingService.getImages(for: contact.dao.pictureUrl(),
                                                withBlock: { response in
                    
                            if let response = response {
                                
                            contact.dao.picture(with: mappingService.deserializeContactImage(response))
                            viewDelegate.updateContent(viewModel: self)
                        }
                    })
                }
            }            
        }
    }
    
    func count() -> Int {
        
        if let models = models {
            
            return models.count
        }
        
        return 0
    }
    
    func name(at index:Int) -> String? {
    
        if let models = models,
            let model = models[index] as? Contact {
            
            return  model.dao.name()
        }
        return .none
    }
    
    func phone(at index:Int) -> String? {
        
        if let models = models,
            let model = models[index] as? Contact {
            
            return  model.dao.phone()
        }
        
        return .none
    }
    
    func addNewEntry() {
        
        if let coordinatorDelegate = coordinatorDelegate {
            
            coordinatorDelegate.addNewContactAction(self)
        }
    }
    
    func selectEntry(at index: Int) {
        
        if let coordinatorDelegate = coordinatorDelegate,
            let models = models,
            index > -1,
            models.count > index {
            
            selectedModel = models[index]
            coordinatorDelegate.selectContactAction(self)
        }
    }
    
    func image(at index: Int) -> Data? {
        
        if let models = models,
            let model = models[index] as? Contact {
            
            return  model.dao.picture()
        }
        
        return .none
    }
}
