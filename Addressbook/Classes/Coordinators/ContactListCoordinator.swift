import UIKit

protocol ContactListCoordinatorDelegate: class {}

class ContactListCoordinator: CoordinatorProtocol {
    
    var isInitialView = false
    weak var delegate: ContactListCoordinatorDelegate?
    var contactDetailCoordinator: ContactDetailCoordinator?
    var createContactCoordinator: CreateContactCoordinator?
    
    var window: UIWindow
    var contactListViewController: ContactListViewController?

    init(window: UIWindow) {
        self.window = window
    }

    func render(withParentViewModel viewModel: ViewModelProtocol?) {
        
        let storyboard = UIStoryboard(name: Constant.Config.StoryboardName, bundle: nil)
        guard let contactListViewController = storyboard.instantiateViewController(withIdentifier: Constant.Config.ContactListViewControllerIdentifier) as? ContactListViewController else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        
        let viewModel =  ContactListViewModel(withParentViewModel: viewModel)
        viewModel.coordinatorDelegate = self
        contactListViewController.viewModel = viewModel
        contactListViewController.navigationBarTitle = Constant.Screen.ContactListTitle
        self.contactListViewController = contactListViewController
        
        if isInitialView {
            
            let navigationController = UINavigationController(rootViewController: contactListViewController)
            window.rootViewController = navigationController
        } else {
            
            guard let root = window.rootViewController,
                     let navigationController = root.navigationController else { fatalError(Constant.ErrorMessage.Unknown.rawValue) }
            navigationController.pushViewController(contactListViewController, animated: true)
        }
    }
}

extension ContactListCoordinator: ContactListViewModelDelegate {
    
    func addNewContactAction(_ viewModel: ViewModelProtocol) {
        
        createContactCoordinator = CreateContactCoordinator(window: window)
        createContactCoordinator?.delegate = self
        createContactCoordinator?.render(withParentViewModel: viewModel)
    }
    
    func selectContactAction(_ viewModel: ViewModelProtocol) {
        
        contactDetailCoordinator = ContactDetailCoordinator(window: window)
        contactDetailCoordinator?.delegate = self
        contactDetailCoordinator?.render(withParentViewModel: viewModel)
    }
}

extension ContactListCoordinator: ContactDetailCoordinatorDelegate {

    func contactDetailCoordinatorDidFinish(contactDetailCoordinator: ContactDetailCoordinator) {
        //do nothing right now
    }
}

extension ContactListCoordinator: CreateContactCoordinatorDelegate {
    
    func createContactCoordinatorDidFinish(createContactCoordinator: CreateContactCoordinator) {
        
        self.createContactCoordinator = nil
        guard let navigationController = window.rootViewController as? UINavigationController else { fatalError(Constant.ErrorMessage.Unknown.rawValue) }
        navigationController.popViewController(animated: true)
    }
}
