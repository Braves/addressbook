import UIKit

protocol CreateContactCoordinatorDelegate: class {

    func createContactCoordinatorDidFinish(createContactCoordinator: CreateContactCoordinator)
}

class CreateContactCoordinator: CoordinatorProtocol {
    
    var isInitialView = false
    weak var delegate: CreateContactCoordinatorDelegate?
    var window: UIWindow
    var createContactViewController: CreateContactViewController?
    
    init(window: UIWindow) {
        
        self.window = window
    }
    
    func render(withParentViewModel viewModel: ViewModelProtocol?) {
        
        let storyboard = UIStoryboard(name: Constant.Config.StoryboardName, bundle: nil)
        guard let createContactViewController = storyboard.instantiateViewController(withIdentifier: Constant.Config.CreateContactViewControllerIdentifier) as? CreateContactViewController else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        
        let viewModel =  CreateContactViewModel(withParentViewModel: viewModel)
        viewModel.coordinatorDelegate = self
        createContactViewController.viewModel = viewModel
        createContactViewController.navigationBarTitle = "Create" // dynamic !!
        self.createContactViewController = createContactViewController
        
        if isInitialView {
            
            let navigationController = UINavigationController(rootViewController: createContactViewController)
            window.rootViewController = navigationController
        } else {
            
            guard let navigationController = window.rootViewController as? UINavigationController else { fatalError(Constant.ErrorMessage.Unknown.rawValue) }
            navigationController.pushViewController(createContactViewController, animated: true)
        }
    }
}

extension CreateContactCoordinator: CreateContactViewModelDelegate{
    
    func saveWithViewModel(_: ViewModelProtocol) {
        delegate?.createContactCoordinatorDidFinish(createContactCoordinator: self)
    }
}
