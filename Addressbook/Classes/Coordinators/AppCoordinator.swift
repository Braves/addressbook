import UIKit

class AppCoordinator: CoordinatorProtocol {
    
    var isInitialView = false

    fileprivate let kContactListCoordinator = "ContactList"
    fileprivate let kContactDetailCoordinator = "ContactDetail"
    fileprivate let kCreateContactCoordinator = "CreateContact"
    
    var window: UIWindow
    var coordinators = [String:CoordinatorProtocol]()
    
    init(window: UIWindow) {
        
        self.window = window
    }

    func render(withParentViewModel viewModel: ViewModelProtocol?) {
        
        showContactList(viewModel)
    }
}

extension AppCoordinator: ContactListCoordinatorDelegate {
    
    func showContactList(_ viewModel: ViewModelProtocol?) {
        
        let contactListCoordinator = ContactListCoordinator(window: window)
        coordinators[kContactListCoordinator] = contactListCoordinator
        contactListCoordinator.delegate = self
        contactListCoordinator.isInitialView = true
        contactListCoordinator.render(withParentViewModel: nil)
    }
}
