import UIKit

protocol ContactDetailCoordinatorDelegate: class {

    func contactDetailCoordinatorDidFinish(contactDetailCoordinator: ContactDetailCoordinator)
}

class ContactDetailCoordinator: CoordinatorProtocol {

    var isInitialView = false
    weak var delegate: ContactDetailCoordinatorDelegate?
    var window: UIWindow
    var contactDetailViewController: ContactDetailViewController?    
    
    init(window: UIWindow) {
        
        self.window = window
    }
    
    func render(withParentViewModel viewModel: ViewModelProtocol?) {
        
        let storyboard = UIStoryboard(name: Constant.Config.StoryboardName, bundle: nil)
        guard let contactDetailViewController = storyboard.instantiateViewController(withIdentifier: Constant.Config.ContactDetailViewControllerIdentifier) as? ContactDetailViewController else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        
        let viewModel =  ContactDetailViewModel(withParentViewModel: viewModel)
        contactDetailViewController.viewModel = viewModel
        contactDetailViewController.navigationBarTitle = "Detail"  // dynamic !!
        self.contactDetailViewController = contactDetailViewController
        
        if isInitialView {
            
            let navigationController = UINavigationController(rootViewController: contactDetailViewController)
            window.rootViewController = navigationController
        } else {
            
            guard let navigationController = window.rootViewController as? UINavigationController else { fatalError(Constant.ErrorMessage.Unknown.rawValue) }
            navigationController.pushViewController(contactDetailViewController, animated: true)
        }        
    }
    
}
